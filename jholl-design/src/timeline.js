$(document).ready(function() {
    function t() {
        $(".timeline").waypoint("destroy"), $(".timeline-item").waypoint("destroy"), $(window).width() > 767 && !Modernizr.touch && ($(".timeline").waypoint(function(t) {
            "down" === t && ($(this).children(".active").removeClass("active"), $(".timeline").removeClass("active"), $(this).addClass("active"), $(".timeline, .timeline-item").removeClass("activeWaypoint"), $(this).addClass("activeWaypoint")), "up" === t && ($(".timeline").removeClass("active"), $(this).children(".active").removeClass("active"), $(this).prev(".timeline").addClass("active"))
        }), $(".timeline-item").waypoint(function(t) {
            "down" === t && ($(this).addClass("active"), $(".timeline, .timeline-item").removeClass("activeWaypoint"), $(this).addClass("activeWaypoint"))
        }, {
            offset: "90%"
        }).waypoint(function(t) {
            "up" === t && ($(this).nextAll().removeClass("active"), $(this).addClass("active"), $(".timeline, .timeline-item").removeClass("activeWaypoint"), $(this).addClass("activeWaypoint"))
        }))
    }

    function e() {
        new Date - n < o ? setTimeout(e, o) : (i = !1, t())
    }! function() {
        var t = [].indexOf || function(t) {
                for (var e = 0, n = this.length; n > e; e++)
                    if (e in this && this[e] === t) return e;
                return -1
            },
            e = [].slice;
        ! function(t, e) {
            return "function" == typeof define && define.amd ? define("waypoints", ["jquery"], function(n) {
                return e(n, t)
            }) : e(t.jQuery, t)
        }(window, function(n, i) {
            var o, r, l, s, a, c, u, h, d, f, p, m, v, w, y, g;
            return o = n(i), h = t.call(i, "ontouchstart") >= 0, s = {
                horizontal: {},
                vertical: {}
            }, a = 1, u = {}, c = "waypoints-context-id", p = "resize.waypoints", m = "scroll.waypoints", v = 1, w = "waypoints-waypoint-ids", y = "waypoint", g = "waypoints", r = function() {
                function t(t) {
                    var e = this;
                    this.$element = t, this.element = t[0], this.didResize = !1, this.didScroll = !1, this.id = "context" + a++, this.oldScroll = {
                        x: t.scrollLeft(),
                        y: t.scrollTop()
                    }, this.waypoints = {
                        horizontal: {},
                        vertical: {}
                    }, this.element[c] = this.id, u[this.id] = this, t.bind(m, function() {
                        var t;
                        return e.didScroll || h ? void 0 : (e.didScroll = !0, t = function() {
                            return e.doScroll(), e.didScroll = !1
                        }, i.setTimeout(t, n[g].settings.scrollThrottle))
                    }), t.bind(p, function() {
                        var t;
                        return e.didResize ? void 0 : (e.didResize = !0, t = function() {
                            return n[g]("refresh"), e.didResize = !1
                        }, i.setTimeout(t, n[g].settings.resizeThrottle))
                    })
                }
                return t.prototype.doScroll = function() {
                    var t, e = this;
                    return t = {
                        horizontal: {
                            newScroll: this.$element.scrollLeft(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left"
                        },
                        vertical: {
                            newScroll: this.$element.scrollTop(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up"
                        }
                    }, !h || t.vertical.oldScroll && t.vertical.newScroll || n[g]("refresh"), n.each(t, function(t, i) {
                        var o, r, l;
                        return l = [], r = i.newScroll > i.oldScroll, o = r ? i.forward : i.backward, n.each(e.waypoints[t], function(t, e) {
                            var n, o;
                            return i.oldScroll < (n = e.offset) && n <= i.newScroll ? l.push(e) : i.newScroll < (o = e.offset) && o <= i.oldScroll ? l.push(e) : void 0
                        }), l.sort(function(t, e) {
                            return t.offset - e.offset
                        }), r || l.reverse(), n.each(l, function(t, e) {
                            return e.options.continuous || t === l.length - 1 ? e.trigger([o]) : void 0
                        })
                    }), this.oldScroll = {
                        x: t.horizontal.newScroll,
                        y: t.vertical.newScroll
                    }
                }, t.prototype.refresh = function() {
                    var t, e, i, o = this;
                    return i = n.isWindow(this.element), e = this.$element.offset(), this.doScroll(), t = {
                        horizontal: {
                            contextOffset: i ? 0 : e.left,
                            contextScroll: i ? 0 : this.oldScroll.x,
                            contextDimension: this.$element.width(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left",
                            offsetProp: "left"
                        },
                        vertical: {
                            contextOffset: i ? 0 : e.top,
                            contextScroll: i ? 0 : this.oldScroll.y,
                            contextDimension: i ? n[g]("viewportHeight") : this.$element.height(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up",
                            offsetProp: "top"
                        }
                    }, n.each(t, function(t, e) {
                        return n.each(o.waypoints[t], function(t, i) {
                            var o, r, l, s, a;
                            return o = i.options.offset, l = i.offset, r = n.isWindow(i.element) ? 0 : i.$element.offset()[e.offsetProp], n.isFunction(o) ? o = o.apply(i.element) : "string" == typeof o && (o = parseFloat(o), i.options.offset.indexOf("%") > -1 && (o = Math.ceil(e.contextDimension * o / 100))), i.offset = r - e.contextOffset + e.contextScroll - o, i.options.onlyOnScroll && null != l || !i.enabled ? void 0 : null !== l && l < (s = e.oldScroll) && s <= i.offset ? i.trigger([e.backward]) : null !== l && l > (a = e.oldScroll) && a >= i.offset ? i.trigger([e.forward]) : null === l && e.oldScroll >= i.offset ? i.trigger([e.forward]) : void 0
                        })
                    })
                }, t.prototype.checkEmpty = function() {
                    return n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical) ? (this.$element.unbind([p, m].join(" ")), delete u[this.id]) : void 0
                }, t
            }(), l = function() {
                function t(t, e, i) {
                    var o, r;
                    "bottom-in-view" === i.offset && (i.offset = function() {
                        var t;
                        return t = n[g]("viewportHeight"), n.isWindow(e.element) || (t = e.$element.height()), t - n(this).outerHeight()
                    }), this.$element = t, this.element = t[0], this.axis = i.horizontal ? "horizontal" : "vertical", this.callback = i.handler, this.context = e, this.enabled = i.enabled, this.id = "waypoints" + v++, this.offset = null, this.options = i, e.waypoints[this.axis][this.id] = this, s[this.axis][this.id] = this, o = null != (r = this.element[w]) ? r : [], o.push(this.id), this.element[w] = o
                }
                return t.prototype.trigger = function(t) {
                    return this.enabled ? (null != this.callback && this.callback.apply(this.element, t), this.options.triggerOnce ? this.destroy() : void 0) : void 0
                }, t.prototype.disable = function() {
                    return this.enabled = !1
                }, t.prototype.enable = function() {
                    return this.context.refresh(), this.enabled = !0
                }, t.prototype.destroy = function() {
                    return delete s[this.axis][this.id], delete this.context.waypoints[this.axis][this.id], this.context.checkEmpty()
                }, t.getWaypointsByElement = function(t) {
                    var e, i;
                    return (i = t[w]) ? (e = n.extend({}, s.horizontal, s.vertical), n.map(i, function(t) {
                        return e[t]
                    })) : []
                }, t
            }(), f = {
                init: function(t, e) {
                    var i;
                    return e = n.extend({}, n.fn[y].defaults, e), null == (i = e.handler) && (e.handler = t), this.each(function() {
                        var t, i, o, s;
                        return t = n(this), o = null != (s = e.context) ? s : n.fn[y].defaults.context, n.isWindow(o) || (o = t.closest(o)), o = n(o), i = u[o[0][c]], i || (i = new r(o)), new l(t, i, e)
                    }), n[g]("refresh"), this
                },
                disable: function() {
                    return f._invoke.call(this, "disable")
                },
                enable: function() {
                    return f._invoke.call(this, "enable")
                },
                destroy: function() {
                    return f._invoke.call(this, "destroy")
                },
                prev: function(t, e) {
                    return f._traverse.call(this, t, e, function(t, e, n) {
                        return e > 0 ? t.push(n[e - 1]) : void 0
                    })
                },
                next: function(t, e) {
                    return f._traverse.call(this, t, e, function(t, e, n) {
                        return e < n.length - 1 ? t.push(n[e + 1]) : void 0
                    })
                },
                _traverse: function(t, e, o) {
                    var r, l;
                    return null == t && (t = "vertical"), null == e && (e = i), l = d.aggregate(e), r = [], this.each(function() {
                        var e;
                        return e = n.inArray(this, l[t]), o(r, e, l[t])
                    }), this.pushStack(r)
                },
                _invoke: function(t) {
                    return this.each(function() {
                        var e;
                        return e = l.getWaypointsByElement(this), n.each(e, function(e, n) {
                            return n[t](), !0
                        })
                    }), this
                }
            }, n.fn[y] = function() {
                var t, i;
                return i = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [], f[i] ? f[i].apply(this, t) : n.isFunction(i) ? f.init.apply(this, arguments) : n.isPlainObject(i) ? f.init.apply(this, [null, i]) : i ? n.error("The " + i + " method does not exist in jQuery Waypoints.") : n.error("jQuery Waypoints needs a callback function or handler option.")
            }, n.fn[y].defaults = {
                context: i,
                continuous: !0,
                enabled: !0,
                horizontal: !1,
                offset: 0,
                triggerOnce: !1
            }, d = {
                refresh: function() {
                    return n.each(u, function(t, e) {
                        return e.refresh()
                    })
                },
                viewportHeight: function() {
                    var t;
                    return null != (t = i.innerHeight) ? t : o.height()
                },
                aggregate: function(t) {
                    var e, i, o;
                    return e = s, t && (e = null != (o = u[n(t)[0][c]]) ? o.waypoints : void 0), e ? (i = {
                        horizontal: [],
                        vertical: []
                    }, n.each(i, function(t, o) {
                        return n.each(e[t], function(t, e) {
                            return o.push(e)
                        }), o.sort(function(t, e) {
                            return t.offset - e.offset
                        }), i[t] = n.map(o, function(t) {
                            return t.element
                        }), i[t] = n.unique(i[t])
                    }), i) : []
                },
                above: function(t) {
                    return null == t && (t = i), d._filter(t, "vertical", function(t, e) {
                        return e.offset <= t.oldScroll.y
                    })
                },
                below: function(t) {
                    return null == t && (t = i), d._filter(t, "vertical", function(t, e) {
                        return e.offset > t.oldScroll.y
                    })
                },
                left: function(t) {
                    return null == t && (t = i), d._filter(t, "horizontal", function(t, e) {
                        return e.offset <= t.oldScroll.x
                    })
                },
                right: function(t) {
                    return null == t && (t = i), d._filter(t, "horizontal", function(t, e) {
                        return e.offset > t.oldScroll.x
                    })
                },
                enable: function() {
                    return d._invoke("enable")
                },
                disable: function() {
                    return d._invoke("disable")
                },
                destroy: function() {
                    return d._invoke("destroy")
                },
                extendFn: function(t, e) {
                    return f[t] = e
                },
                _invoke: function(t) {
                    var e;
                    return e = n.extend({}, s.vertical, s.horizontal), n.each(e, function(e, n) {
                        return n[t](), !0
                    })
                },
                _filter: function(t, e, i) {
                    var o, r;
                    return (o = u[n(t)[0][c]]) ? (r = [], n.each(o.waypoints[e], function(t, e) {
                        return i(o, e) ? r.push(e) : void 0
                    }), r.sort(function(t, e) {
                        return t.offset - e.offset
                    }), n.map(r, function(t) {
                        return t.element
                    })) : []
                }
            }, n[g] = function() {
                var t, n;
                return n = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [], d[n] ? d[n].apply(null, t) : d.aggregate.call(null, n)
            }, n[g].settings = {
                resizeThrottle: 100,
                scrollThrottle: 30
            }, o.on("load.waypoints", function() {
                return n[g]("refresh")
            })
        })
    }.call(this), t(), $(".timeline-section-window img").mousedown(function() {
        return !1
    });
    var n = new Date(1, 1, 2e3, 12, 0, 0),
        i = !1,
        o = 400;
    $(window).resize(function() {
        n = new Date, i === !1 && (i = !0, setTimeout(e, o))
    })
});