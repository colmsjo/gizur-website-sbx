Introduction
============

This web-site is hosted out of a S3 bucket.

Instructions for setup: http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html

Use the script publish.sh to publish new versions (requires S3cmd and with credentials configured).




Development
-----------

Base URL for testing: http://www.gizur.com.sbx.s3-website-eu-west-1.amazonaws.com

I use this [tool](http://www.eslinstructor.net/vkbeautify/) for beautifying and minimizing (when I'm lazy not buidling with grunt): 

Gizur logo colors:

 * Green - A3B846 (dark) to 6B9131 (light)
 * Blue - 265F9C (dark) to  51CFE8 (light)
 * Light blue - 00A5B8 (dark) to 79DFDF (light)
 * Orange - D78042 (dark) to BBAA53 (light)


The colors used in the web site, `grep -i -n [color] *`:

 * A3B846 - backgrounds etc.
 * D78042 - article 1
 * 212121 - article 2 and article 4
 * 6B9131 & A3B846 - article 3

Icons used: http://shoestrap.org/downloads/elusive-icons-webfont/


Images
------

See image_sizes.png for an illustation of the different images sizes and targeted devices.

widths:
 * Size & resolution			- Porttraint/Landscape - Notes								- CSS
 * 767 & DPI 130				- L 					- mobiles								- default
 * 767 x 2 (1534) & DPI 326		- L 					- tablet portrait (high resolution)	- data-media="(min-device-pixel-ratio: 2.0)
 * 960 & DPI 130				- P 					- desktop and tablet landscape		- data-media="(min-width: 768px)
 * 960 x 2 (1920) & DPI 326		- P 					- large desktops (high resolution)	- data-media="(min-width: 768px) and (min-device-pixel-ratio: 2.0)




