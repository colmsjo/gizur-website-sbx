The colors used in the web site:

 * 06A4B7  -> A3B846 - backgrounds etc.
 * BE3587 & 7B2156 -> D78042 - article 1
 * 212121 - article 2 and article 4
 * 265D5F & EEFCFD -> 6B9131 & FAFCF1 - article 3

 * 19AABC 


Referenser

Extremt duktiga på detaljhandeln. Gizur har förståelse för logistik och dagligvarubranchen ända ner i detaljnivå. Hög kompetens och levererar alltid i tid. Håller dessutom alltid kostnadsramarna.

Thomas Säll/CIO Coop Inköp och Kategori, Coop


Verksamheten i fokus. Gizur kan dagligvarubranschen och har alltid ett väldigt tydligt verksamhetsperspektiv. Verksamhetens krav sätts i fokus och kopplas sedan till IT-lösningar. Vi har ett stort förtroende för Gizur.
Susanne Thelander, Ansvarig för Cikab IT-Roadmap på Coop Inköp och Kategori


Stort engagemang. Gög integritet. Driver projekt för företagets bästa och inte bara som ett uppdrag. Kompetent och kreativt.

Per Rosengren, Ansvarig för Kvalitet och Miljö, KF