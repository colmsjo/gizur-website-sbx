Getting started
--------------

Just open index.html in a browser.


Development
----------

Install Bower and lesscss (assuming you alread have node installed): `npm install -g bower` and ghen `npm install -g less`.

Compile the less stylesheet: `lessc GGS.less assets/style.css`

Less stylesheet adapted to em's: `lessc GGS2.less assets/style.css`

